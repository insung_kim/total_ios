//
//  ViewController.swift
//  Total
//
//  Created by bodyfriend_dev on 2017. 7. 4..
//  Copyright © 2017년 bodyfriend_dev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadingV: UIView!
    
    func loadWebPage(_ url: String){
        let myurl = URL(string: url)
        let myRequest = URLRequest(url: myurl!)
        webView.loadRequest(myRequest)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loadWebPage("http://t.bodyfriend.co.kr/m/")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        let url = request.url!.absoluteString
        print(url)
        if url.contains("promotionNotice.view") {
            openLink(url)
            return false
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loadingV.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingForDelay()
    }
    
    func openLink(_ string: String) {
        let url = URL(string: string)!
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func loadingForDelay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadingV.isHidden = true
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
}

